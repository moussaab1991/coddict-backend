const http = axios.create({
  baseURL: "http://localhost:8000/api/auth",
  timeout: 1000,
  headers: { "Content-Type": "application/json" }
});

window.browser = (function() {
  return window.msBrowser || window.browser || window.chrome;
})();

function getUrlWithKey(text) {
  console.log("after send url ", text);
  let data = { search: text };
  http
    .post("/links", data)
    .then(response => {
      console.log("response", response);
      urlData = response.data;
    })
    .catch(error => {
      if (error.message === "Network Error") alert("check the connection ");
      else if (error.response.status === 401)
        alert("please login before using application");
      console.log("error", error.message);
    });
}
function logInUser(userData, cb) {
  console.log("logIn user Data: ", userData);
  http
    .post("/login", userData)
    .then(response => {
      let userToken = response.data.access_token;
      console.log("the token is ", userToken);
      // LocalStorage.setStorage({ token: userToken });
      http.defaults.headers.common["Authorization"] = `Bearer ${userToken}`;
      cb();
    })
    .catch(error => {
      if (error.message === "Network Error") alert("check the connection ");
      else if (error.response.status === 422) alert("wrong user name ");
      else if (error.response.status === 422) alert("wrong password");
      console.log("error", error.message);
    });
}

function logOut() {
  console.log("logout");
  http
    .get("/logout")
    .then(response => {
      // LocalStorage.setStorage({ token: userToken });
      http.defaults.headers.common["Authorization"] = "";
      urlData = [];
    })
    .catch(error => {
      if (error.message === "Network Error") alert("check the connection ");

      console.log("error", error.message);
    });
}

let urlData = [];

browser.runtime.onMessage.addListener((request, sender, sendResponse) => {
  if (request.msg === "logging_in") {
    console.log("the data from popup message is", request.data);

    logInUser(request.data, () => {
      browser.browserAction.setPopup({
        popup: "/popup/signOut.html"
      });
    });
  }
  if (request.msg === "logging_out") {
    browser.browserAction.setPopup({
      popup: "/popup/popupLogin.html"
    });

    logOut();
  }

  sendResponse({
    response: "Message received"
  });
});

browser.omnibox.onInputChanged.addListener((text, suggest) => {
  getUrlWithKey(text);
  console.log(urlData);
  const map = urlData.map((value, index) => {
    console.log(index);
    return {
      content: value.url_key,
      description: value.url_key + "     =>      " + value.url
    };
  });

  console.log(map);

  suggest(map);
});

browser.omnibox.onInputEntered.addListener(text => {
  console.log("ad", text);
  var result = urlData.find(value => {
    return value.url_key === text;
  });
  console.log("heloo", result);
  browser.tabs.update({ url: result.url });
});
