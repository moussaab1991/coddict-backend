// // export function logMessage (text) {
// //   console.log('logMessage: ' + text)
// // }

// export default function Apple(type) {
//   this.type = type;
//   this.color = "red";
//   this.getInfo = getAppleInfo;
// }

// // anti-pattern! keep reading...
// function getAppleInfo() {
//   return this.color + " " + this.type + " apple";
// }
class Person {
  constructor(name) {
    this._name = name;
  }

  get name() {
    return this._name.toUpperCase();
  }

  set name(newName) {
    this._name = newName; // validation could be checked here such as only allowing non numerical values
  }

  walk() {
    console.log(this._name + " is walking.");
  }
}
export default Person;
