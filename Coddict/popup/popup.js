window.browser = (function() {
  return window.msBrowser || window.browser || window.chrome;
})();

function sendMassage(messageName, messageData) {
  chrome.runtime.sendMessage(
    {
      msg: messageName,
      data: messageData
    },
    function(response) {
      // console.log("reponse from background is" + response.response);
      window.close();
    }
  );
}

$(function() {
  $("#signIn").click(function() {
    var formData = {
      email: $("#username").val(),
      password: $("#password").val()
      // remember_me: true
    };
    sendMassage("logging_in", formData);
  });
});
