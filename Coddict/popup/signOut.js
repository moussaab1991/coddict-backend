function sendMassage(messageName, messageData) {
  chrome.runtime.sendMessage(
    {
      msg: messageName,
      data: messageData
    },
    function(response) {
      // console.log("reponse from background is" + response.response);
      window.close();
    }
  );
}

$(function() {
  $("#signOut").click(function() {
    sendMassage("logging_out");
  });
});
