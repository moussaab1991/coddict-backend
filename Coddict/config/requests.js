import LocalStorage from "./localStorage.js";

const http = axios.create({
  baseURL: "http://localhost:8000/api/auth",
  timeout: 1000,
  headers: { "Content-Type": "application/json" }
});

http.interceptors.request.use(
  function(config) {
    let token = "";

    LocalStorage.getStorage(items => {
      console.log(items.token);
      token = items.token;
    });
    console.log("token is ", token);
    if (token) config.headers.Authorization = `Bearer ${token}`;
    return config;
  },
  function(error) {
    return Promise.reject(error);
  }
);

export default http;
