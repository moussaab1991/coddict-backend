import axiosRequest from "./requests.js";
import LocalStorage from "./localStorage.js";

export function logIn(userData) {
  console.log("logIn user Data: ", userData);
  axiosRequest.post("/login", userData).then(response => {
    let userToken = response.data.access_token;
    console.log("the token is ", userToken);
    LocalStorage.setStorage({ token: userToken });
  });
}

export function logOut() {
  console.log("logMessage: " + text);
  axiosRequest.get("/logout").then(response => {});
}
