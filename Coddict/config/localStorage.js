class LocalStorage {
  setStorage(items) {
    chrome.storage.sync.set(items, function() {
      console.log("Settings saved");
    });
  }

  removeStorage() {
    chrome.storage.sync.clear(function() {
      var error = chrome.runtime.lastError;
      if (error) {
        console.error(error);
      }
    });
  }
  getStorage(cb) {
    chrome.storage.sync.get(null, function(items) {
      if (chrome.runtime.lastError) {
        console.log("jiou", chrome.runtime.lastError);
        cb(chrome.runtime.lastError);
      }
      cb(items);
    });
  }

  checkStorage(cname) {
    if (localStorage.getItem(cname) === null) {
      return false;
    } else return true;
  }
}
export default new LocalStorage();
